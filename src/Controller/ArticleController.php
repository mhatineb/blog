<?php

namespace App\Controller;

use App\Entities\Article;
use App\Repository\ArticleRepo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/article')]
class ArticleController extends AbstractController
    {
    public function __construct(private ArticleRepo $repo)
    {
        $this->repo = $repo;
    }

    #[Route( methods:'GET')]
    public function all(Request $request): JsonResponse
    {
        
        return $this->json(
            $this->repo->findAll()
        );
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $article = $this->repo->findById($id);
        if(!$article){
            throw new NotFoundHttpException();

        }
        return $this->json($article);
    }


    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer,ValidatorInterface $validator ) {
        try {

        $article = $serializer->deserialize($request->getContent(), Article::class, 'json');
        
        $this->repo->persist($article);
        
        return $this->json($article, Response::HTTP_CREATED);
    } catch (ValidationFailedException $e) {
        return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
    } catch (NotEncodableValueException $e) {
        return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
    }



    }
    
    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator) {   
        $article = $this->repo->findById($id);
        
        if (!$article) {
            throw new NotFoundHttpException(); 
        }
        try {
        $toUpdate = $serializer->deserialize($request->getContent(), Article::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
    } catch (ValidationFailedException $e) {
        return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
    } catch (NotEncodableValueException $e) {
        return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
    }

       } 


    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $article = $this->repo->findById($id);
           if(!$article){
               throw new NotFoundHttpException();
           }
   
           $this->repo->delete($article);
   
           return $this->json(null, Response::HTTP_NO_CONTENT);
           
       }
      
    }

    


