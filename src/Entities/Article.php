<?php


namespace App\Entities;


class Article
{
	
    private ?int $id;

	private ?string $name;
	#[Assert\NotBlank]

    private ?string $title;
	#[Assert\NotBlank]

    private ?string $picture;
	#[Assert\NotBlank]

    private ?string $description;
	#[Assert\NotBlank]

	
  
 
	/**
	 * @param int|null $id
	 * @param string|null $name
	 * @param string|null $title
	 * @param string|null $picture
	 * @param string|null $description
	 */
	public function __construct(?string $name, ?string $title, ?string $picture, ?string $description,?int $id=null) {
		$this->id = $id;
		$this->name = $name;
		$this->title = $title;
		$this->picture = $picture;
		$this->description = $description;
		
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
		/**
	 * @return string|null
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param string|null $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param string|null $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
	

	/**
	 * @return string|null
	 */
	public function getPicture(): ?string {
		return $this->picture;
	}
	
	/**
	 * @param string|null $picture 
	 * @return self
	 */
	public function setPicture(?string $picture): self {
		$this->picture = $picture;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string {
		return $this->description;
	}
	
	/**
	 * @param string|null $description 
	 * @return self
	 */
	public function setDescription(?string $description): self {
		$this->description = $description;
		return $this;
	}	

}