
Via MySQL/MariaDb 
J'ai créé 3 tables: -article: pour manipuler des articles dans mon blog
                    -selection: pour utiliser une sélection d'articles, en page d'accueil, fait par la rédaction (non modifiables ni supprimables).
                    -sortie: pour utiliser une sélection d'articles dans une page "sorties du mois"(non modifiables ni supprimables).

J'ai entré des valeurs dans la table "article" car j'ai préféré me concentrer sur une seule table.

Via PHP/Symfony

J'ai créé  pour la table "article": 
           - l'entity pour créé le construct et les getters/setters
           - le repository pour créé les fonctionnalités pour consulter, modifier et supprimer la base de donnée avec les bindValue pour la sécurité.
           - le controller pour créé les routes vers le front en react/next.js
