<?php

namespace App\Repository;

use App\Entities\Article;
use PDO;


class ArticleRepo
{

    private PDO $connection;

    
    public function __construct() {
        // $this->connection = Database::connect();
        $this->connection = new PDO('mysql:host=localhost;dbname=blog', 'simplon', '1234');
    
    }    

    public function findAll()
    {
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM article');
        $statement->execute();
        
        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $articles[] = $this->sqlToArticle($item);
        }

        return $articles;
    }

    public function findById(int $id):?Article {
        $statement = $this->connection->prepare('SELECT * FROM article WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToArticle($result);
        }
        return null;
    }




    public function persist(Article $article)
    {
        $statement = $this->connection->prepare('INSERT INTO article (name,title,picture,description) VALUES (:name,:title, :picture, :description)');
            $statement->bindValue('name', $article->getName());
            $statement->bindValue ('title', $article->getTitle());
            $statement->bindValue('picture',$article->getPicture());
            $statement->bindValue('description', $article->getDescription());
            
            $statement->execute();

            $article->setId($this->connection->lastInsertId());
    }

    
    public function update(Article $article): void
    {
        $statement = $this->connection->prepare('UPDATE article SET name=:name, title=:title, picture=:picture, description=:description WHERE id=:id');
        $statement->bindValue(":name", $article->getName(),PDO::PARAM_STR);
        $statement->bindValue(":title", $article->getTitle(), PDO::PARAM_STR);
        $statement->bindValue(':picture', $article->getPicture(), PDO::PARAM_STR);
        $statement->bindValue(":description", $article->getDescription(), PDO::PARAM_STR);
        
        $statement->bindValue(':id', $article->getId(), PDO::PARAM_INT);
        
       
        $statement->execute();

    }

    public function delete(Article $article): void
    {
        $query = $this->connection->prepare("DELETE FROM article WHERE id=:id");
        $query->bindValue('id', $article->getId(), PDO::PARAM_INT);
        $query->execute();
    }

    private function sqlToArticle(array $line):Article {
        
        return new Article($line['name'],$line['title'], $line['picture'], $line['description'], $line['id']);
    }

}

